import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CadImovelComponent } from './components/cad-imovel/cad-imovel.component';
import { CadImobiliariaComponent } from './components/cad-imobiliaria/cad-imobiliaria.component';
import { CadProprietarioComponent } from './components/cad-proprietario/cad-proprietario.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TopoNavComponent } from './components/topo-nav/topo-nav.component';
import { FuncionarioService } from './services/funcionario/funcionario.service';
import { ImobiliariaService } from './services/imobiliaria/imobiliaria.service';
import { FormsModule } from '@angular/forms';

import { ClientesService } from './services/clientes/clientes.service';
import { ImoveisService } from './services/imoveis/imoveis.service';
import { ContratoProprietarioService } from './services/contrato-proprietario/contrato-proprietario.service';
import { ContratoClienteService } from './services/contrato-cliente/contrato-cliente.service';

@NgModule({
  declarations: [
    AppComponent,
    CadImovelComponent,
    CadImobiliariaComponent,
    CadProprietarioComponent,
    TopoNavComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    FuncionarioService,
    ImobiliariaService,
    ClientesService,
    ImoveisService,
    ContratoProprietarioService,
    ContratoClienteService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
