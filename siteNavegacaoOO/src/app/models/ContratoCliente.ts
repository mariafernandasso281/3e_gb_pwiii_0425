export class ContratoCliente {
  id: number
  id_cliente: number
  id_contrato_imobiliaria: number
  data_inicio : Date
  data_termino : Date
  valor_combinado : number
  tipo : string
  fiador : string
  ativo : boolean
}