import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    pathMatch:'full',
    redirectTo: 'home'
  },
  { path: 'home',
    loadChildren: () => import('./components/home/home.module')
     .then(m => m.HomeModule)
  },
  {
    path: 'cad-imobiliaria',
      loadChildren: () => import('./components/cad-imobiliaria/cad-imobiliaria.module')
        .then(m => m.CadImobiliariaModule)
  },
  {
    path: 'cad-imovel',
      loadChildren: () => import ('./components/cad-imovel/cad-imovel.module')
        .then(m => m.CadImovelModule)
  },
  {
    path: 'cad-proprietario',
      loadChildren: () => import ('./components/cad-proprietario/cad-proprietario.module')
        .then(m => m.CadProprietarioModule)
  },
  {
    path: 'cad-funcionario',
      loadChildren: () => import ('./components/cad-funcionario/cad-funcionario.module')
        .then(m => m.CadFuncionarioModule)
  },
  {
    path: 'cad-contrato-proprietario',
    loadChildren: () => import ('./components/cad-contrato-proprietario/cad-contrato-proprietario.module')
      .then(m => m.CadContratoProprietarioModule)
  },
  {
    path: 'cad-contrato-cliente',
    loadChildren: () => import ('./components/cad-contrato-cliente/cad-contrato-cliente.module')
    .then(m => m.CadContratoClienteModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
