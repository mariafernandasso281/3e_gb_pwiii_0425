import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Cliente } from 'src/app/models/Cliente';

@Injectable({
  providedIn: 'root'
})
export class ClientesService {

  private readonly URL_E = "https://3000-mariafernan-3egbapi0810-uhrrqkokn7p.ws-us78.gitpod.io/"
  private readonly URL_M = ""
  private readonly URL = this.URL_E

  constructor(private http: HttpClient) { }

  getClientes(): Observable<any> {
    return this.http.get<Cliente>(`${this.URL}clientes`)
  } 

  postClientes(cliente: Cliente): Observable<any> {
    return this.http.post<Cliente>(`${this.URL}cliente/usuario`, cliente)
  }
}
