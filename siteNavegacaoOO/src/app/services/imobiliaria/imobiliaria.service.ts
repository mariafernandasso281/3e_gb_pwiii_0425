import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Imobiliaria } from 'src/app/models/Imobiliaria';

@Injectable({
  providedIn: 'root'
})
export class ImobiliariaService {
  
  private readonly URL_E = ""
  private readonly URL_M = "https://3000-mariafernan-3egbapi0810-peii3qn6wau.ws-us79.gitpod.io/"
  private readonly URL = this.URL_M

  constructor(private httpClient: HttpClient) { }

  buscarImobiliarias():Observable<any>{
    return this.httpClient.get<any>(`${this.URL}imobiliarias`)
  }

  postImobiliaria(imobiliaria: Imobiliaria): Observable<any> {
    return this.httpClient.post<Imobiliaria>(`${this.URL}imobiliaria/pessoa`, imobiliaria)
  }

}
