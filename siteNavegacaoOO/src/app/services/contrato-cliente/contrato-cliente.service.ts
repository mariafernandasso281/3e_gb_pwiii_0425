import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ContratoCliente } from 'src/app/models/ContratoCliente';

@Injectable({
  providedIn: 'root'
})
export class ContratoClienteService {

  private readonly URL_E = ""
  private readonly URL_M = "https://3000-mariafernan-3egbapi0810-peii3qn6wau.ws-us79.gitpod.io/"
  private readonly URL = this.URL_M

  constructor(private http: HttpClient) { }

  getContratosCliente(): Observable<any> {
    return this.http.get<ContratoCliente>(`${this.URL}contratoimobiliariacliente`)
  }

  postContratosCliente(contrato: ContratoCliente): Observable<any> {
    return this.http.post<ContratoCliente>(`${this.URL}contratoimobiliariacliente`, contrato)
  }
}

