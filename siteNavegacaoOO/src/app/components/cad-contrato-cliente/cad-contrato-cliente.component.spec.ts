import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CadContratoClienteComponent } from './cad-contrato-cliente.component';

describe('CadContratoClienteComponent', () => {
  let component: CadContratoClienteComponent;
  let fixture: ComponentFixture<CadContratoClienteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CadContratoClienteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CadContratoClienteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
