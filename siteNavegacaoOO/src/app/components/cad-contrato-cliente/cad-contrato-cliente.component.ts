import { Component, OnInit } from '@angular/core';
import { ContratoCliente } from 'src/app/models/ContratoCliente';
import { ContratoClienteService } from 'src/app/services/contrato-cliente/contrato-cliente.service';

@Component({
  selector: 'app-cad-contrato-cliente',
  templateUrl: './cad-contrato-cliente.component.html',
  styleUrls: ['./cad-contrato-cliente.component.scss']
})
export class CadContratoClienteComponent implements OnInit {

  contrato: ContratoCliente
  constructor(private contratosService: ContratoClienteService) { 
    this.contrato = new ContratoCliente()
  }

  ngOnInit(): void {
    this.exibirContratos()
  }

  exibirContratos(): void {
    this.contratosService.getContratosCliente().subscribe({
      next: (data) => {
        console.log(data)
      },
      error: (error) => {
        console.error(error)
      }
    })
  }

  cadastrarContrato(): void {
    this.contrato.id_contrato_imobiliaria = 2
    this.contrato.id_cliente = 12
    this.contratosService.postContratosCliente(this.contrato).subscribe({
      next: (data) => {
        console.log(data)
      },
      error: (error) => {
        console.error(error)
      }
    })
  }
}
