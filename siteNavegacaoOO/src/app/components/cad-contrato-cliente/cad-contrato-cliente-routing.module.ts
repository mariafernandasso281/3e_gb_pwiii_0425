import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CadContratoClienteComponent } from './cad-contrato-cliente.component';

const routes: Routes = [
  {
    path: '',
    component: CadContratoClienteComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CadContratoClienteRoutingModule { }
