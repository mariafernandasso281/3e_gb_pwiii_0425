import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CadContratoClienteRoutingModule } from './cad-contrato-cliente-routing.module';
import { CadContratoClienteComponent } from './cad-contrato-cliente.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    CadContratoClienteComponent
  ],
  imports: [
    CommonModule,
    CadContratoClienteRoutingModule,
    FormsModule
  ]
})
export class CadContratoClienteModule { }
