import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CadContratoProprietarioRoutingModule } from './cad-contrato-proprietario-routing.module';
import { CadContratoProprietarioComponent } from './cad-contrato-proprietario.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    CadContratoProprietarioComponent
  ],
  imports: [
    CommonModule,
    CadContratoProprietarioRoutingModule,
    FormsModule
  ]
})
export class CadContratoProprietarioModule { }
