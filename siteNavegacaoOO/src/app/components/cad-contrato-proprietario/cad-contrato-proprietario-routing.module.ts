import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CadContratoProprietarioComponent } from './cad-contrato-proprietario.component';

const routes: Routes = [
  {
    path: '',
    component: CadContratoProprietarioComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CadContratoProprietarioRoutingModule { }
