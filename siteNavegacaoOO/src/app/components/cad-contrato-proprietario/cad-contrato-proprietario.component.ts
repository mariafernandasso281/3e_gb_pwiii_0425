import { Component, OnInit } from '@angular/core';
import { ContratoProprietario } from 'src/app/models/ContratoProprietario';
import { ContratoProprietarioService } from 'src/app/services/contrato-proprietario/contrato-proprietario.service';

@Component({
  selector: 'app-cad-contrato-proprietario',
  templateUrl: './cad-contrato-proprietario.component.html',
  styleUrls: ['./cad-contrato-proprietario.component.scss']
})
export class CadContratoProprietarioComponent implements OnInit {

  contrato: ContratoProprietario
  constructor(private contratosService: ContratoProprietarioService) { 
    this.contrato = new ContratoProprietario()
  }

  ngOnInit(): void {
    this.exibirContratos()
  }

  exibirContratos(): void {
    this.contratosService.getContratosProprietario().subscribe({
      next: (data) => {
        console.log(data)
      },
      error: (error) => {
        console.error(error)
      }
    })
  }

  cadastrarContrato(): void {
    this.contrato.id_imobiliaria = 3
    this.contrato.id_imovel = 12
    this.contratosService.postContratosProprietario(this.contrato).subscribe({
      next: (data) => {
        console.log(data)
      },
      error: (error) => {
        console.error(error)
      }
    })
  }
}
