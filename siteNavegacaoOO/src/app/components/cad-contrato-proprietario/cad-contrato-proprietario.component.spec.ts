import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CadContratoProprietarioComponent } from './cad-contrato-proprietario.component';

describe('CadContratoProprietarioComponent', () => {
  let component: CadContratoProprietarioComponent;
  let fixture: ComponentFixture<CadContratoProprietarioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CadContratoProprietarioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CadContratoProprietarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
