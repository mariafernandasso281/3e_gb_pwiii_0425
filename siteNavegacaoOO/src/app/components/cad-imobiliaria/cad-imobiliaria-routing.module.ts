import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CadImobiliariaComponent } from './cad-imobiliaria.component';

const routes: Routes = [
  {
    path: '',
    component: CadImobiliariaComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CadImobiliariaRoutingModule { }
