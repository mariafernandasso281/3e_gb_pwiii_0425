import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CadImobiliariaRoutingModule } from './cad-imobiliaria-routing.module';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CadImobiliariaRoutingModule,
    FormsModule
  ]
})
export class CadImobiliariaModule { }
