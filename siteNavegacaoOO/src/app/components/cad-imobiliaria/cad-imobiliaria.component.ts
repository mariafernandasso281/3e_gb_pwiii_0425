import { Component, OnInit } from '@angular/core';
import { Imobiliaria } from 'src/app/models/Imobiliaria';
import { ImobiliariaService } from 'src/app/services/imobiliaria/imobiliaria.service';

@Component({
  selector: 'app-cad-imobiliaria',
  templateUrl: './cad-imobiliaria.component.html',
  styleUrls: ['./cad-imobiliaria.component.scss']
})
export class CadImobiliariaComponent implements OnInit {

  imobiliaria : Imobiliaria
  constructor(
    private imobiliariaService: ImobiliariaService
  ) {
    this.imobiliaria = new Imobiliaria()
  }

  ngOnInit(): void {
    this.getImobiliaria()
  }

  getImobiliaria(): void {
    this.imobiliariaService.buscarImobiliarias().subscribe({
      next: (data) => {
        console.log(data)
      },
      error: (error) => {
        console.error(error.message)
      }
    })
  }


  cadastrarImobiliaria(): void {
    this.imobiliaria.tipo = 'jurídica'
    this.imobiliariaService.postImobiliaria(this.imobiliaria).subscribe({
      next: (data) => {
        console.log(data)
      },
      error: (error) => {
        console.error(error)
      }
    })
  }

  

}
