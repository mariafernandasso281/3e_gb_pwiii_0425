import { Component, OnInit } from '@angular/core';
import { Cliente } from 'src/app/models/Cliente';
import { ClientesService } from 'src/app/services/clientes/clientes.service';

@Component({
  selector: 'app-cad-proprietario',
  templateUrl: './cad-proprietario.component.html',
  styleUrls: ['./cad-proprietario.component.scss']
})
export class CadProprietarioComponent implements OnInit {

  public cliente: Cliente
  constructor(private clientesService: ClientesService) {
    this.cliente = new Cliente()
  }

  ngOnInit(): void {
    this.exibirClientes()
  }

  exibirClientes(): void {
    this.clientesService.getClientes().subscribe({
      next: (data) => {
        console.log(data)
      },
      error: (error) => {
        console.error(error)
      }
    })
  }

  cadastrarCliente(): void {
    this.clientesService.postClientes(this.cliente).subscribe({
      next: (data) => {
        console.log(data)
      },
      error: (error) => {
        console.error(error)
      }
    })
  }

}
