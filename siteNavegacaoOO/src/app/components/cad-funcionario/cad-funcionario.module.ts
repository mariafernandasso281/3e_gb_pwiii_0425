import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CadFuncionarioRoutingModule } from './cad-funcionario-routing.module';
import { CadFuncionarioComponent } from './cad-funcionario.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    CadFuncionarioComponent
  ],
  imports: [
    CommonModule,
    CadFuncionarioRoutingModule,
    FormsModule
  ]
})
export class CadFuncionarioModule { }
