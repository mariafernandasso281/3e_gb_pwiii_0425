import { Component, OnInit } from '@angular/core';
import { Funcionario } from 'src/app/models/Funcionario';
import { FuncionarioService } from 'src/app/services/funcionario/funcionario.service';

@Component({
  selector: 'app-cad-funcionario',
  templateUrl: './cad-funcionario.component.html',
  styleUrls: ['./cad-funcionario.component.scss']
})
export class CadFuncionarioComponent implements OnInit {

  funcionario: Funcionario
  constructor(
    private funcionarioService: FuncionarioService
  ) {
    this.funcionario = new Funcionario()
   }


  ngOnInit(): void {
    this.getFuncionarios()
  }

  getFuncionarios(): void {
    this.funcionarioService.buscarFuncionarios().subscribe({
      next: (data) => {
        console.log(data)
      },
      error: (error) => {
        console.error(error.message)
      }
    })
  }

  cadastrarFuncionario(): void {
    this.funcionarioService.postFuncionario(this.funcionario).subscribe({
      next: (data) => {
        console.log(data)
      },
      error: (error) => {
        console.error(error)
      }
    })
  }

}
