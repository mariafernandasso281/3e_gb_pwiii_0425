import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-topo-nav',
  templateUrl: './topo-nav.component.html',
  styleUrls: ['./topo-nav.component.scss']
})
export class TopoNavComponent implements OnInit {

  paginas =[
    {
      path: "",
      text: "Home"
    },
    {
      path: "/cad-imovel",
      text: "Cadastrar Imóvel"
    },
    {
      path: "/cad-imobiliaria",
      text: "Cadastrar Imobiliária"
    },
    {
      path: "/cad-proprietario",
      text: "Cadastrar Proprietário"
    },
    {
      path: '/cad-funcionario',
      text: "Cadastrar Funcionário"
    },
    {
      path: "/cad-contrato-proprietario",
      text: "Cadastrar Contrato de Proprietário"
    },
    {
      path: "/cad-contrato-cliente",
      text: "Cadastrar Contrato de Cliente com Imobiliária"
    }
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
