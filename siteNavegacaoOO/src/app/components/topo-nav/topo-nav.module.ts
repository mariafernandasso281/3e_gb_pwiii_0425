import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TopoNavComponent } from './topo-nav.component';



@NgModule({
  declarations: [
    TopoNavComponent
  ],
  imports: [
    CommonModule
  ]
})
export class TopoNavModule { }
