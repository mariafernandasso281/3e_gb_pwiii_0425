import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TopoNavComponent } from './topo-nav.component';

describe('TopoNavComponent', () => {
  let component: TopoNavComponent;
  let fixture: ComponentFixture<TopoNavComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TopoNavComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TopoNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
